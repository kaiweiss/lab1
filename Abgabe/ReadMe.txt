Abgabe Lab1 Kai Weiß (76454)

Anmerkungen:

1. - Bitte die .rar-Datei im selbem Ordner ("Abgabe/Blender") entpacken. (Leider erlaubt Gitlab nichts über 10Mb.)
   - Die Blender-Datei braucht die Materialien in seinem Ordner, ansonsten findet es diese nicht. 
   - Die Animationen sind unterschiedlich lang. Um sie in Blender dann anzuschauen sollten folgende Endframenummer eingestellt werden:
	-Idle: 240
	-Walking: 40
	-Shooting: 140
  
2. - Auch die Fbx braucht die Materialen in seinem Ordner.

3. - Das Unitypackage braucht die Materialen in seinem Ordner.